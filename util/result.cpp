//
// Created by Petar on 3/30/2019.
//

#include <bits/stdc++.h>
#include "result.h"

using namespace std;

string result::toString(bool includeCycleDetails){

    stringstream ss{};
    ss << "(" << endl;
    ss << algorithmName << endl;
    ss << cycleCost << endl;

    if (includeCycleDetails){
        ss << cycle.size() << endl;
        for (int i=0;i<cycle.size();i++){
            ss << cycle[i] << " ";
        }
        ss << endl;
    }

    ss << counters.size() << endl;
    for (int i=0;i<counters.size();i++){
        ss << counters[i].first << endl;
        ss << counters[i].second << endl;
    }

    ss << ")";
    return ss.str();
}

string result::json(){

    stringstream ss{};
    ss << "{";

    ss << "\"algorithmName\":\"";
    ss << algorithmName;
    ss << "\"";
    ss << ",";


    ss << "\"cycleCost\":";
    ss << cycleCost;
    ss << ",";

    ss << "\"cycle\":[";
    for (int i=0;i<cycle.size();i++) {
        ss << cycle[i];
        if (i<cycle.size()-1) ss << ",";
    }
    ss << "]";
    ss << ",";

    ss << "\"counters\":{";
    for (int i=0;i<counters.size();i++) {
        ss << "\""+counters[i].first+"\":";
        ss << counters[i].second;
        if (i<counters.size()-1) ss << ",";
    }
    ss << "}";

    ss << "}";
    return ss.str();
}