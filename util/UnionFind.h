//
// Created by Petar on 3/31/2019.
//

#ifndef HAMCYCLE_UNIONFIND_H
#define HAMCYCLE_UNIONFIND_H

#include <iostream>
#include <random>
#include <time.h>
#include <algorithm>
#include <chrono>
#include <stack>

using namespace std;

class UnionFind{
private:
    vector<int> P;
    std::default_random_engine rndForUF;
    std::discrete_distribution<int> distForUF;
public:
    UnionFind(int n){
        P=vector<int>(n);
        for (int i=0;i<n;i++) P[i]=i;
        rndForUF=std::default_random_engine{};
        distForUF=std::discrete_distribution<int>(0,1);
    }
    int parent(int i);
    void join(int i, int j);
};

#endif //HAMCYCLE_UNIONFIND_H
