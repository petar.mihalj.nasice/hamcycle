//
// Created by Petar on 3/31/2019.
//

#include "UnionFind.h"

int UnionFind::parent(int i){
    if (P[i]==i) return i;
    return P[i]=parent(P[i]);
}

void UnionFind::join(int i, int j){
    if (distForUF(rndForUF)%2==0){
        P[parent(i)]=parent(j);
    }
    else{
        P[parent(j)]=parent(i);
    }
}
