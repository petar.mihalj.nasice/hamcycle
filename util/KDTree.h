#ifndef HAMCYCLE_KDTREE_H
#define HAMCYCLE_KDTREE_H

#include <bits/stdc++.h>
#include "point.h"
using namespace std;

struct Node{
    Node *left,*right;
    point p;
    int index;
    bool active;
};

class KDTree{
private:
    Node* root;
    int stepCount;
    vector<Node*> pointNodeFinder;
    Node* buildR(vector<pair<point,int>>& data, int depth, int a, int b);
    pair<Node*,double> closestR(Node* root, point q, int depth);
    Node* build(vector<point>& data);
public:
    KDTree(vector<point> data){
        pointNodeFinder=vector<Node*>(data.size());
        root = build(data);
        stepCount = 0;
    }
    KDTree(){

    }
    bool getActiveState(int i);
    void setActiveState(int i, bool state);
    int getStepCount(){return stepCount;}
    int closest(point q);
    point get(int index){return pointNodeFinder[index]->p;}
};

#endif //HAMCYCLE_KDTREE_H
