#include "KDTree.h"
using namespace std;

Node* KDTree::buildR(vector<pair<point,int>>& data, int depth, int a, int b){
    if (a>b) return nullptr;

    sort(data.begin()+a,data.begin()+b+1,
         [&](pair<point,int> p1, pair<point,int> p2)-> bool{
            return (depth%2==0?p1.first.x:p1.first.y)<(depth%2==0?p2.first.x:p2.first.y);
         }
    );

    int m=(a+b)/2;
    Node* n = new Node{
            buildR(data,depth+1,a,m-1),
            buildR(data,depth+1,m+1,b),
            data[m].first,
            data[m].second,
            true
    };
    this->pointNodeFinder[data[m].second]=n;
    return n;
}

Node* KDTree::build(vector<point>& data){
    vector<pair<point,int>> dataNew{};
    for (int i=0;i<data.size();i++){
        dataNew.push_back({data[i],i});
    }
    return buildR(dataNew, 0, 0, data.size()-1);
}

pair<Node*,double> KDTree::closestR(Node* root, point q, int depth){
    double distToMe=(root->active)?distSq(root->p,q):10E50;

    stepCount++;
    pair<Node*,double> leftOpt{nullptr,10E50};
    pair<Node*,double> rightOpt{nullptr,10E50};

    if ((depth%2==0?q.x:q.y)<(depth%2==0?root->p.x:root->p.y)){
        //left
        if (root->left){
            leftOpt=closestR(root->left,q,depth+1);
            double d=abs((depth%2==0?q.x:q.y)-
                    (depth%2==0?root->p.x:root->p.y)
            ); //CHECK
            if (root->right && d*d<leftOpt.second){
                rightOpt=closestR(root->right,q,depth+1);
            }
        }
        else if (root->right){
            rightOpt=closestR(root->right,q,depth+1);
        }
    }
    else{
        //right
        if (root->right){
            rightOpt=closestR(root->right,q,depth+1);
            double d=abs((depth%2==0?q.x:q.y)-
                         (depth%2==0?root->p.x:root->p.y)
            ); //CHECK
            if (root->left && d*d<rightOpt.second){
                leftOpt=closestR(root->left,q,depth+1);
            }
        }
        else if (root->left){
            leftOpt=closestR(root->left,q,depth+1);
        }
    }
    if (leftOpt.second<distToMe && leftOpt.second<=rightOpt.second){
        return leftOpt;
    }
    else if (rightOpt.second<distToMe && rightOpt.second<leftOpt.second){
        return rightOpt;
    }
    else{
        return {root,distToMe};
    }
}

int KDTree::closest(point q){
    int ind = closestR(this->root,q,0).first->index;
    return ind;
}

bool KDTree::getActiveState(int i){
    return this->pointNodeFinder[i]->active;
}
void KDTree::setActiveState(int i, bool state){
    this->pointNodeFinder[i]->active=state;
}
