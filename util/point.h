//
// Created by Petar on 3/30/2019.
//

#ifndef HAMCYCLE_POINT_H
#define HAMCYCLE_POINT_H

struct point{
    double x,y;
};

double distSq(point p1, point p2);

#endif //HAMCYCLE_POINT_H
