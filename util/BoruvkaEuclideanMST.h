//
// Created by Petar on 4/2/2019.
//

#ifndef HAMCYCLE_BORUVKAEUCLIDEANMST_H
#define HAMCYCLE_BORUVKAEUCLIDEANMST_H

#include <bits/stdc++.h>
#include "result.h"
#include "point.h"

vector<vector<int>> boruvkaEuclideanMST(vector<point> v);

#endif //HAMCYCLE_BORUVKAEUCLIDEANMST_H
