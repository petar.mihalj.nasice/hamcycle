//
// Created by Petar on 3/30/2019.
//

#include "point.h"

double distSq(point p1, point p2){
    return (p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y);
}