//
// Created by Petar on 3/30/2019.
//

#ifndef HAMCYCLE_RESULT_H
#define HAMCYCLE_RESULT_H

#include <bits/stdc++.h>
using namespace std;

class result{
public:
    string algorithmName;
    double cycleCost;
    vector<int> cycle;
    vector<pair<string,int>> counters;
    string toString(bool includeCycleDetails);
    string json();
};



#endif //HAMCYCLE_RESULT_H
