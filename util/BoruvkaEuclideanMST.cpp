//
// Created by Petar on 4/2/2019.
//

#include "BoruvkaEuclideanMST.h"
#include "UnionFind.h"
#include "KDTree.h"

vector<vector<int>> boruvkaEuclideanMST(vector<point> v){
    int n = v.size();
    UnionFind UF(n);
    KDTree T(v);

    set<pair<int,int>> sol{};
    vector<vector<int>> frags(n);
    int realFragsSize=n;
    for (int i=0;i<n;i++){
        frags[i].push_back(i);
    }

    while (realFragsSize>1){
        for (int i=0;i<n;i++){
            if (frags[i].size()==0) continue;

            //disable current fragment
            for (int frag:frags[i]){
                T.setActiveState(frag,false);
            }

            //calculate closest edge
            int clInd=-1;
            int clIndSource=-1;
            double clIndDist=10E50;
            for (int frag:frags[i]){
                int curr=T.closest(T.get(frag));
                double d=distSq(T.get(curr),T.get(frag));
                if (d<clIndDist){
                    clIndDist=d;
                    clInd=curr;
                    clIndSource=frag;
                }
            }
            UF.join(clIndSource,clInd);
            sol.insert({clIndSource,clInd});
            sol.insert({clInd,clIndSource});


            //enable current fragment
            for (int frag:frags[i]){
                T.setActiveState(frag,true);
            }
        }
        //update fragments
        frags=vector<vector<int>>(n);
        realFragsSize=0;
        for (int i=0;i<n;i++){
            int parent=UF.parent(i);
            if (frags[parent].size()==0){
                realFragsSize++;
            }
            frags[parent].push_back(i);
        }
    }

    vector<vector<int>> solVector(n);
    for (pair<int,int> p:sol){
        solVector[p.first].push_back(p.second);
    }

    return solVector;
}