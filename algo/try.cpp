#include <iostream>
#include <random>
#include <time.h>
#include <algorithm>
#include <chrono>
#include <stack>

using namespace std;

std::default_random_engine rnd{15};
std::uniform_real_distribution<double> rd(0,100);

std::default_random_engine rndForUF{};
std::discrete_distribution<int> distForUF(0,1);

struct DoublePoint{
    double x,y;
};

DoublePoint getRnd(){
    return DoublePoint{rd(rnd),rd(rnd)};
}

struct IntPoint{
    int x,y;
};

double getL0(vector<DoublePoint>& points){
    double minX=+10E50;
    double minY=+10E50;
    double maxX=-10E50;
    double maxY=-10E50;

    for (DoublePoint p:points){
        minX=min(minX,p.x);
        minY=min(minY,p.y);
        maxX=min(maxX,p.x);
        maxY=min(maxY,p.y);
    }

    return max(maxX-minX,maxY-minY);
}

vector<IntPoint> peturbate(vector<DoublePoint>& points, double c){
    int n = points.size();
    double L0 = getL0(points);

    double scale = L0/(64*c*n);

    vector<IntPoint> res{};

    for (DoublePoint p: points){
        res.push_back(IntPoint{(int)round(p.x/scale),(int)round(p.y/scale)});
    }
    for (IntPoint& p: res){
        p.x=p.x*2-1;
        p.y=p.y*2-1;
    }
    return res;
}




int main(){
    int n=5000;
    vector<Point> v{};
    for (int i=0;i<n;i++){
        v.emplace_back(getRnd());

    }




    auto t1 = chrono::high_resolution_clock::now();
    auto t2 = chrono::high_resolution_clock::now();

    cout << "MST time: " << chrono::duration_cast<chrono::milliseconds>(t2-t1).count() << endl;
}